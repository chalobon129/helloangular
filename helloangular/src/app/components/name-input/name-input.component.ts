import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-name-input',
  templateUrl: './name-input.component.html',
  styleUrls: ['./name-input.component.css'],
})
export class NameInputComponent implements OnInit {
  title = 'Hello';
  name: string;
  constructor() {}

  ngOnInit(): void {}
}
